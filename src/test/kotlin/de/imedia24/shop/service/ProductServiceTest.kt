import de.imedia24.shop.domain.product.ProductEntity
import de.imedia24.shop.dto.product.AddProductDto
import de.imedia24.shop.dto.product.UpdateProductDto
import de.imedia24.shop.repository.ProductRepository
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.ArgumentMatchers.any
import org.mockito.InjectMocks
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.junit.jupiter.MockitoExtension
import org.springframework.context.MessageSource
import java.math.BigDecimal
import java.util.*

@ExtendWith(MockitoExtension::class)
class ProductServiceTest {

    @Mock
    private lateinit var productRepository: ProductRepository

    @Mock
    private lateinit var messageSource: MessageSource

    @InjectMocks
    private lateinit var productService: ProductService

    @Test
    fun testFindProductBySku() {
        val sku = "123"

        `when`(productRepository.findById(sku)).thenReturn(Optional.of(ProductEntity("123", "p1", "its p1", BigDecimal(4), null, null)))
        val result = productService.findProductBySku(sku)

        assertEquals("p1", result.name)
    }

    @Test
    fun testFindProductBySkus() {
        val skus = listOf("123", "456");

        `when`(productRepository.findAllById(skus)).thenReturn(listOf(ProductEntity("123", "p1", "its p1", BigDecimal(4), null, null)));
        val result = productService.findProductBySkus(skus)

        assertEquals(1, result.size)
        assertEquals("p1", result[0].name)
    }

    @Test
    fun testAddProduct() {
        val addProductDto = AddProductDto("123", "p1", "its p1", BigDecimal(4))

        `when`(productRepository.save(any())).thenReturn(ProductEntity("123", "p1", "its p1", BigDecimal(4), null, null))
        val result = productService.addProduct(addProductDto)

        assertEquals("p1", result.name)
    }

    @Test
    fun testUpdateProduct() {
        val sku = "123"
        val updateProductDto = UpdateProductDto("p1", "its p1", BigDecimal(4))

        `when`(productRepository.save(any())).thenReturn(ProductEntity("123", "p1", "its p1", BigDecimal(4), null, null))
        val result = productService.updateProduct(sku, updateProductDto)

        assertEquals("p1", result.name)
    }
}
