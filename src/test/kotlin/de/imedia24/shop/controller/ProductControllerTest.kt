import com.fasterxml.jackson.databind.ObjectMapper
import de.imedia24.shop.controller.ProductController
import de.imedia24.shop.dto.product.AddProductDto
import de.imedia24.shop.dto.product.ProductResponseDto
import de.imedia24.shop.dto.product.UpdateProductDto
import de.imedia24.shop.service.ProductService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.math.BigDecimal

@SpringBootTest(classes = [ProductController::class])
@AutoConfigureMockMvc
class ProductControllerTest {

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var productService: ProductService

    val objectMapper = ObjectMapper()

    //@Test
    fun testFindProductsBySku() {

        val sku = "123"
        val productResponseDto = ProductResponseDto("123", "p1", "its p1", BigDecimal(4))
        `when`(productService.findProductBySku(sku)).thenReturn(productResponseDto)

        mockMvc.perform(get("/v1/api/product/{sku}", sku))
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.sku").value(productResponseDto.sku))
    }

   // @Test
    fun testAddProduct() {

        val addProductDto = AddProductDto("123", "p1", "its p1", BigDecimal(4))
        val productResponseDto = ProductResponseDto("123", "p1", "its p1", BigDecimal(4))
        `when`(productService.addProduct(addProductDto)).thenReturn(productResponseDto)

        mockMvc.perform(
                post("/v1/api/product")
                        .content(objectMapper.writeValueAsString(addProductDto))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.sku").value(productResponseDto.sku))
    }

   // @Test
    fun testUpdateProduct() {
        val sku = "123"
        val updateProductDto = UpdateProductDto("p1", "its p1", BigDecimal(4))
        val productResponseDto = ProductResponseDto("123", "p1", "its p1", BigDecimal(4))
        `when`(productService.updateProduct(sku, updateProductDto)).thenReturn(productResponseDto)


        mockMvc.perform(
                put("/v1/api/product/{sku}", sku)
                        .content(objectMapper.writeValueAsString(updateProductDto))
                        .contentType(MediaType.APPLICATION_JSON)
        )
                .andExpect(status().isOk)
                .andExpect(jsonPath("$.sku").value(productResponseDto.sku))
    }
}
