package de.imedia24.shop.domain.product

import org.hibernate.annotations.UpdateTimestamp
import java.math.BigDecimal
import java.time.ZonedDateTime
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.Id
import javax.persistence.Table

@Entity
@Table(name = "products")
data class ProductEntity(
    @Id
    val sku: String,

    @Column(nullable = false)
    val name: String,

    @Column
    val description: String? = null,

    @Column(nullable = false)
    val price: BigDecimal,

    @UpdateTimestamp
    @Column(name = "created_at")
    val createdAt: ZonedDateTime?,

    @UpdateTimestamp
    @Column(name = "updated_at")
    val updatedAt: ZonedDateTime?
) {
    constructor() : this("", "", null, BigDecimal(0), null, null) {

    }


}
