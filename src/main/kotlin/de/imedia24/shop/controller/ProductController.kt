package de.imedia24.shop.controller

import de.imedia24.shop.dto.error.ApiError
import de.imedia24.shop.dto.product.AddProductDto
import de.imedia24.shop.dto.product.ProductResponseDto
import de.imedia24.shop.dto.product.UpdateProductDto
import de.imedia24.shop.service.ProductService
import de.imedia24.shop.utils.GlobalConst
import io.swagger.v3.oas.annotations.Operation
import io.swagger.v3.oas.annotations.media.Content
import io.swagger.v3.oas.annotations.media.Schema
import io.swagger.v3.oas.annotations.responses.ApiResponse
import io.swagger.v3.oas.annotations.responses.ApiResponses
import io.swagger.v3.oas.annotations.tags.Tag
import org.slf4j.LoggerFactory
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@Tag(name = GlobalConst.PRODUCT_TAG, description = GlobalConst.PRODUCT_APIS_DESCRIPTION)
@RequestMapping("v1/api/")
@RestController
class ProductController(private val productService: ProductService) {


    private val logger = LoggerFactory.getLogger(ProductController::class.java)!!

    @Operation(
            summary = "Retrieve a product by sku",
            description = "Get a product object by specifying its sku. The response is a product object with sku, name, description, and price."
    )
    @ApiResponses(
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = ProductResponseDto::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "500", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")])
    )
    @GetMapping("/product/{sku}", produces = ["application/json;charset=utf-8"])
    fun findProductsBySku(@PathVariable sku: String): ProductResponseDto {
        logger.info("Start API : Request for product $sku")
        val product = productService.findProductBySku(sku)
        logger.info("End API : Request for product $sku")
        return product;
    }

    @Operation(
            summary = "Retrieve list of product by list of sku",
            description = "Get a product list  by specifying a list of sku. The response is list of : sku, name, description, and price."
    )
    @ApiResponses(
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = Array<ProductResponseDto>::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "500", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")])
    )
    @GetMapping("/products")
    fun findProductsBySkus(@RequestParam skus: List<String>): List<ProductResponseDto> {
        logger.info("Start API : Request for products $skus")
        val product = productService.findProductBySkus(skus)
        logger.info("End API : Request for products $skus")
        return product;
    }

    @Operation(
            summary = "Add a product",
            description = "Add a product (sku, name, description, and price)."
    )
    @ApiResponses(
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = ProductResponseDto::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "500", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")])
    )
    @PostMapping("/product")
    fun addProduct(@RequestBody @Valid addProductDto: AddProductDto): ProductResponseDto {
        logger.info("Start API :adding product $(addProductDto.sku)")
        val product = productService.addProduct(addProductDto)
        logger.info("End API : adding product $(addProductDto.sku)")
        return product;
    }


    @Operation(
            summary = "Update a product",
            description = "Update a product (name, description, and price)."
    )
    @ApiResponses(
            ApiResponse(responseCode = "200", content = [Content(schema = Schema(implementation = ProductResponseDto::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "400", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")]),
            ApiResponse(responseCode = "500", content = [Content(schema = Schema(implementation = ApiError::class), mediaType = "application/json")])
    )
    @PutMapping("/product/{sku}")
    fun updateProduct(@RequestBody @Valid updateProductDto: UpdateProductDto, @PathVariable sku: String): ProductResponseDto {
        logger.info("Start API :updating product $sku")
        val product = productService.updateProduct(sku, updateProductDto)
        logger.info("End API : updating product $sku")
        return product;
    }
}
