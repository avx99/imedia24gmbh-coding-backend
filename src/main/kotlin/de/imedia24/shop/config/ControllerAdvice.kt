package de.imedia24.shop.config

import de.imedia24.shop.dto.error.ApiError
import de.imedia24.shop.exception.BusinessException
import de.imedia24.shop.exception.TechnicalException
import de.imedia24.shop.utils.GlobalConst
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import java.util.Date

@RestControllerAdvice
class ControllerAdvice {
    @ExceptionHandler(BusinessException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun onBusinessException(ex: BusinessException): ApiError {

        return ApiError(
                HttpStatus.BAD_REQUEST.value(),
                GlobalConst.BUSINESS_EXCEPTION_TITLE,
                Date(),
                ex.message,
                null,
                null //TODO : map errors in case of exception of input validation
        )
    }

    @ExceptionHandler(value = [TechnicalException::class, RuntimeException::class])
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun onRuntimeException(ex: Throwable): ApiError {

        return ApiError(
                HttpStatus.BAD_REQUEST.value(),
                GlobalConst.SERVER_EXCEPTION_TITLE,
                Date(),
                ex.message,
                "mocked for now",
                null //TODO : map errors in case of exception of input validation
        )
    }
}
