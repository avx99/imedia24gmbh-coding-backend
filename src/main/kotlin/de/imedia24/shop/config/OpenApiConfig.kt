package de.imedia24.shop.config

import io.swagger.v3.oas.models.OpenAPI
import io.swagger.v3.oas.models.info.Contact
import io.swagger.v3.oas.models.info.Info
import io.swagger.v3.oas.models.info.License
import io.swagger.v3.oas.models.servers.Server
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration


@Configuration
class OpenApiConfig {
    @Value("\${openapi.url.dev}")
    private val devUrl: String? = null

    @Value("\${openapi.url.uat}")
    private val uatUrl: String? = null

    @Value("\${openapi.url.preprod}")
    private val preprodUrl: String? = null

    @Value("\${openapi.url.prod}")
    private val prodUrl: String? = null


    @Bean
    fun myOpenAPI(): OpenAPI {
        val devServer = Server()
        devServer.url = devUrl
        devServer.description = "Server URL in Development environment"
        val uatServer = Server()
        devServer.url = devUrl
        devServer.description = "Server URL in User Acceptance Testing environment"
        val preprodServer = Server()
        devServer.url = devUrl
        devServer.description = "Server URL in Pre-Prod environment"
        val prodServer = Server()
        prodServer.url = prodUrl
        prodServer.description = "Server URL in Production environment"
        val contact = Contact()
        contact.email = "dev.oussama.abouzid@gmail.com"
        contact.name = "Oussama"
        contact.url = "https://www.abouzidoussama.com/"
        val mitLicense: License = License().name("MIT License").url("https://choosealicense.com/licenses/mit/")
        val info: Info = Info()
                .title("Product Management API")
                .version("1.0.0")
                .contact(contact)
                .description("This API exposes endpoints to manage products.").termsOfService("https://www.abouzidoussama.com/terms")
                .license(mitLicense)
        return OpenAPI().info(info).servers(listOf(devServer, uatServer, preprodServer, prodServer))
    }

}