package de.imedia24.shop.config

import org.springframework.context.MessageSource
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.support.ReloadableResourceBundleMessageSource


@Configuration
class MessageSourceConfiguration {

    @Bean
    fun messageConfiguration(): MessageSource {
        val source = ReloadableResourceBundleMessageSource ()
        source.setBasenames("classpath:messages")
        source.setUseCodeAsDefaultMessage(true)
        source.setDefaultEncoding("UTF-8")
        return source
    }
}