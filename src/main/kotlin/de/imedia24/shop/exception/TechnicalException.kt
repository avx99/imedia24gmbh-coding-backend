package de.imedia24.shop.exception

class TechnicalException(override val message: String) : Exception(message) {
}