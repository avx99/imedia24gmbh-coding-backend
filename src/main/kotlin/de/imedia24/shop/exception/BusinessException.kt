package de.imedia24.shop.exception

data class BusinessException(override val message: String) : Exception(message) {
}