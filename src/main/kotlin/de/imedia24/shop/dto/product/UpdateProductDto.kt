package de.imedia24.shop.dto.product

import de.imedia24.shop.domain.product.ProductEntity
import java.math.BigDecimal
import javax.validation.constraints.NotNull

data class UpdateProductDto(
        @NotNull val name: String,
        val description: String?,
        @NotNull val price: BigDecimal
) {
    companion object {
        fun UpdateProductDto.toProductEntity(sku: String) = ProductEntity(
                sku = sku,
                name = name,
                description = description ?: "",
                price = price,
                createdAt = null,
                updatedAt = null
        )
    }
}