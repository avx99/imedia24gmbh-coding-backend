package de.imedia24.shop.dto.product

import de.imedia24.shop.domain.product.ProductEntity
import java.math.BigDecimal
import javax.validation.constraints.NotNull

data class AddProductDto(
        @NotNull val sku: String,
        @NotNull val name: String,
        val description: String,
        @NotNull val price: BigDecimal
) {
    companion object {
        fun AddProductDto.toProductEntity() = ProductEntity(
                sku = sku,
                name = name,
                description = description,
                price = price, null, null
        )
    }
}