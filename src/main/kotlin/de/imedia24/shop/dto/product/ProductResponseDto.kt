package de.imedia24.shop.dto.product

import de.imedia24.shop.domain.product.ProductEntity
import java.math.BigDecimal

data class ProductResponseDto(
    val sku: String,
    val name: String,
    val description: String,
    val price: BigDecimal
) {
    companion object {
        fun ProductEntity.toProductResponse() = ProductResponseDto(
            sku = sku,
            name = name,
            description = description ?: "",
            price = price
        )
    }
}
