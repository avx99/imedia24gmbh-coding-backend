package de.imedia24.shop.dto.error

data class ItemError(val title: String,
                     val description: String) {
}