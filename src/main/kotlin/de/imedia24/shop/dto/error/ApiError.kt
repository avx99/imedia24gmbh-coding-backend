package de.imedia24.shop.dto.error

import java.util.Date


data class ApiError(val status: Int,
                    val title: String,
                    val date: Date,
                    val description: String?,
                    val traceId: String?,
                    val errors: List<ItemError>?) {

}