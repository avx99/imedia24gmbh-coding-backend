package de.imedia24.shop.service

import de.imedia24.shop.dto.product.AddProductDto
import de.imedia24.shop.dto.product.AddProductDto.Companion.toProductEntity
import de.imedia24.shop.dto.product.ProductResponseDto
import de.imedia24.shop.dto.product.ProductResponseDto.Companion.toProductResponse
import de.imedia24.shop.dto.product.UpdateProductDto
import de.imedia24.shop.dto.product.UpdateProductDto.Companion.toProductEntity
import de.imedia24.shop.exception.BusinessException
import de.imedia24.shop.repository.ProductRepository
import de.imedia24.shop.utils.CustomMessages
import org.slf4j.LoggerFactory
import org.springframework.context.MessageSource
import org.springframework.stereotype.Service
import java.util.*
import javax.transaction.Transactional

@Service
class ProductService(private val productRepository: ProductRepository, private val messageSource: MessageSource) {
    private val logger = LoggerFactory.getLogger(ProductService::class.java)!!
    fun findProductBySku(sku: String): ProductResponseDto {
        logger.info("Start service : Request for product $sku");
        val product = productRepository
                .findById(sku)
                .orElseThrow{ BusinessException(messageSource.getMessage(CustomMessages.PRODUCT_NOT_FOUND, arrayOf(sku), Locale.ENGLISH)) };

        val output = product.toProductResponse();
        logger.info("End service : Request for product $sku");
        return output;
    }

    fun findProductBySkus(skus: List<String>): List <ProductResponseDto> {
        logger.info("Start service : Request for products $skus");
        val productIterable = productRepository.findAllById(skus);
        val  output = productIterable.map { productEntity ->  productEntity.toProductResponse()}
        logger.info("End service : Request for products $skus");
        return output;
    }

    fun addProduct(addProductDto: AddProductDto): ProductResponseDto {
        logger.info("Start service :adding product $addProductDto.skus")
        val product = productRepository.save(addProductDto.toProductEntity());
        val  output = product.toProductResponse();
        logger.info("End service :adding product $addProductDto.skus")
        return output;
    }

    fun updateProduct(sku: String, updateProductDto: UpdateProductDto): ProductResponseDto {
        logger.info("Start service : updating product $updateProductDto.skus")
        val product = productRepository.save(updateProductDto.toProductEntity(sku));
        val  output = product.toProductResponse();
        logger.info("End service :updating product $updateProductDto.skus")
        return output;
    }
}
