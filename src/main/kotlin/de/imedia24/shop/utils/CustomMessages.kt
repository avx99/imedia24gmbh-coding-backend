package de.imedia24.shop.utils

class CustomMessages {
    companion object {
        const val PRODUCT_NOT_FOUND = "product.error.not.found"
    }
}