package de.imedia24.shop.utils

class GlobalConst {
    // SWAGGER DOC CONSTANTS
    companion object {
        const val PRODUCT_TAG = "PRODUCT"
        const val PRODUCT_APIS_DESCRIPTION = "Product management APIs"
        const val BUSINESS_EXCEPTION_TITLE = "error/business-exception"
        const val SERVER_EXCEPTION_TITLE = "error/internal-error"
    }
}