package de.imedia24.shop.repository

import de.imedia24.shop.domain.product.ProductEntity
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface ProductRepository : CrudRepository<ProductEntity, String> {
}